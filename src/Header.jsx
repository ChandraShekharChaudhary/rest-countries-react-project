import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMoon } from "@fortawesome/free-solid-svg-icons";

export default function Header({ darkTheam, setDarkMode }) {
  function handleTheam(event) {
    event.target.childNodes[0].data === "Dark Mode"
      ? ((event.target.childNodes[0].data = "Light Mode"), setDarkMode(true))
      : ((event.target.childNodes[0].data = "Dark Mode"), setDarkMode(false));
  }
  return (
    <div className={`header shadow ${darkTheam}`}>
      <div className="header-title">Where is the world?</div>
      <div className="theam-change-container">
        <FontAwesomeIcon icon={faMoon} />
        <div className="theam-change" onClick={(event) => handleTheam(event)}>
          Dark Mode
        </div>
      </div>
    </div>
  );
}
