import { useEffect, useState } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import axios from "axios";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTriangleExclamation } from "@fortawesome/free-solid-svg-icons";


import Header from "./Header";
import FilterSection from "./FilterSection";
import CardsHandler from "./CardsHandler";
import Details from "./Details";

import "./App.css";

function App() {
  const [countries, setCountries] = useState([]);
  const [searchInput, setSearchInput] = useState("");
  const [region, setRegion] = useState("");
  const [subRegion, setSubRegion] = useState("");
  const [isLoading, setLoading] = useState(true);
  const [error, setError] = useState("");
  const [isDarkMode, setDarkMode] = useState(false);
  // Context

  useEffect(() => {
    handleAPI("https://restcountries.com/v3.1/all");
  }, [isLoading]);

  async function handleAPI(url) {
    try {
      let res = await axios.get(url);
      const data = res.data;
      setCountries(data);
      setLoading(false);
    } catch (error) {
      setLoading(false);
      setError(error.message);
      console.log("error message :- ", error);
    }
  }
  const filteredCountries = countries.filter((country) => {
    return (
      country.name.common.toLowerCase().startsWith(searchInput.toLowerCase()) &&
      (region === "" || country.region.toLowerCase() === region) &&
      (subRegion === "" || (country.subregion ? country.subregion.toLowerCase() === subRegion : false))
    );
  });

  console.log(filteredCountries);

  return (
    <>
      {error ? (
        <div className="errorMessage">
          <FontAwesomeIcon
            icon={faTriangleExclamation}
            className="error-icon"
          />{" "}
          <h3>{error}</h3>
        </div>
      ) : (
        <div id="all" className={isDarkMode ? "dark" : ""}>
          <Header
            setDarkMode={setDarkMode}
            darkTheam={isDarkMode ? "darkMode" : ""}
          />
          <Routes>
            <Route
              path="/"
              element={
                <>
                  <FilterSection
                    setSearchInput={setSearchInput}
                    setRegion={setRegion}
                    setSubRegion={setSubRegion}
                    allData={countries}
                    darkTheam={isDarkMode ? "darkMode placeholder" : ""}
                    region={region}
                  />
                  <CardsHandler
                    countries={filteredCountries}
                    isLoading={isLoading}
                    darkTheam={isDarkMode ? "darkMode" : ""}
                    isDarkMode={isDarkMode}
                  />
                </>
              }
            />

            <Route
              path="/country/:id"
              element={<Details isDarkMode={isDarkMode} />}
            />
            <Route
              path="/error"
              element={
                <div className={`invalidUrl ${isDarkMode ? "whiteFont" : ""}`}>
                  <span>Invalid URL</span>
                </div>
              }
            />
          </Routes>
        </div>
      )}
    </>
  );
}

export default App;
