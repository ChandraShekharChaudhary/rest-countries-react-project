import { Link } from "react-router-dom";

export default function CountryCard({ country, darkTheam }) {
  return (
    <div className={`card shadow ${darkTheam}`}>
      <Link className={`cardLink ${darkTheam}`} to={`/country/${country.cca3}`}>
        <img src={country.flags.png} alt="" className="flag" />
        <div className="country-detail-container">
          <h3 className="country-name">{country.name.common}</h3>
          <div className="detail">
            <p className="population">
              {" "}
              <span>Population:</span> {country.population}
            </p>
            <p className="region">
              {" "}
              <span>Region:</span> {country.region}
            </p>
            <p className="capital">
              <span>Capital:</span> {country.capital}
            </p>
          </div>
        </div>
      </Link>
    </div>
  );
}
