import CountryCard from "./CountryCard";

export default function CardsHandler({ countries, isLoading, darkTheam, isDarkMode }) {
  console.log("hey",isDarkMode)
  return (
    <div className="countries-container">
      {isLoading ? (
        <div className={`loadingDiv ${darkTheam}`} key="loadingDiv">
          <span>Loading....</span>
        </div>
      ) : (countries?.length<=0 ? <p className={`notMatchFound ${isDarkMode ? "whiteFont" : ""}`}>search not found . . .</p> :
        countries.map((country, index) => (
          <CountryCard key={index} country={country} darkTheam={darkTheam} />
        ))
      )}
      {/* {isLoading && <></>}
      {countries.length && } */}
    </div>
  );
}
