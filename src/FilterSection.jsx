import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMagnifyingGlass } from "@fortawesome/free-solid-svg-icons";

export default function FilterSection({
  setSearchInput,
  region,
  setRegion,
  allData,
  darkTheam,
  setSubRegion,
}) {
  function handleRegion(event) {
    console.log(event.target.value);
    setRegion(event.target.value);
    setSubRegion("");
  }
  function handleSubRegion(event){
    console.log(event.target.value);
    setSubRegion(event.target.value);
  }

  function handleFilter(event) {
    console.log(event.target.value);
    setSearchInput(event.target.value);
  }

  

  return (
    <div className="filter-section">
      <div className={`searchContainer shadow ${darkTheam}`}>
        <FontAwesomeIcon icon={faMagnifyingGlass} />
        <input
          type="text"
          className={`input-country-name ${darkTheam}`}
          placeholder="Search for a country..."
          onKeyUp={handleFilter}
        />
      </div>
      <div className="filterSec">
        <select
          name="regions"
          className={`select-region shadow ${darkTheam}`}
          onChange={handleRegion}
        >
          <option value="">Filter by Region</option>
          {[
            ...allData.reduce((acc, country) => {
              return acc.add(country.region);
            }, new Set()),
          ].map((region) => (
            <option value={region.toLowerCase()} key={region}>
              {region}
            </option>
          ))}
        </select>

        <select
          name="subRegions"
          className={`select-region shadow ${darkTheam}`}
          onChange={handleSubRegion}
        >
          <option value="">Sub Region</option>
          {[
            ...allData.reduce((acc, country) => {
              return country.region.toLowerCase().includes(region) && country.subregion && acc.add(country.subregion.toLowerCase()) || acc;
            }, new Set()),
          ].map((region) => (
            <option value={region.toLowerCase()} key={region}>
              {region}
            </option>
          ))}
        </select>

      </div>
    </div>
  );
}
