import axios from "axios";
import { useState, useEffect } from "react";
import {
  useParams,
  Link,
  Route,
  Navigate,
  useNavigate,
} from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowLeftLong } from "@fortawesome/free-solid-svg-icons";

export default function Details({ isDarkMode }) {
  const [country, setCountry] = useState("");
  const [isLoading, setLoading] = useState(true);
  const navi = useNavigate();

  let { id } = useParams();
  useEffect(() => {
    getCountryData(id);
  }, [id]);

  async function getCountryData(id) {
    try {
      const res = await axios.get(`https://restcountries.com/v3.1/alpha/${id}`);
      console.log("data", res.data[0]);
      setCountry(res.data[0]);
      setLoading(false);
    } catch (error) {
      console.log("data", error);
      setLoading(false);
      navi("/error");
    }
  }

  return (
    <>
      {isLoading ? (
        <div
          className={`loadingDiv ${isDarkMode ? "whiteFont" : ""}`}
          key="loadingDiv"
        >
          <span>Loading....</span>
        </div>
      ) : (
        <>
          <div className={`detailPage ${isDarkMode ? "whiteFont" : ""}`}>
            <Link
              className={`backButton shadow ${
                isDarkMode && "coloreWhite darkMode"
              }`}
              to="/"
            >
              <FontAwesomeIcon icon={faArrowLeftLong} />
              Back
            </Link>
            <div className="detailContainer">
              <img src={country.flags.png} alt="" />
              <div className="details">
                <h1 className="name">{country.name.common}</h1>
                <div className="info">
                  <div className="sm-1">
                    Native Name:{" "}
                    <p>
                      {(country.name.nativeName &&
                        Object.values(country.name.nativeName).length > 0 &&
                        Object.values(country.name.nativeName)[0].common) ||
                        "NA"}
                    </p>
                  </div>
                  <div className="sm-1">
                    Population: <p>{country.population}</p>
                  </div>
                  <div className="sm-1">
                    Region: <p>{country.region}</p>
                  </div>
                  <div className="sm-1">
                    Sub Region: <p>{country.subregion}</p>
                  </div>
                  <div className="sm-1">
                    Capital:{" "}
                    <p>{(country.capital && country.capital[0]) || "NA"}</p>
                  </div>
                  <div className="sm-1 sm-2">
                    Top Level Domain: <p>{country.tld}</p>
                  </div>
                  <div className="sm-1">
                    Currencies:{" "}
                    <p>
                      {(country.currencies &&
                        Object.values(country.currencies)[0].name) ||
                        "NA"}
                    </p>
                  </div>
                  <div className="sm-1">
                    Langauges:{" "}
                    <p>
                      {(country.languages &&
                        Object.values(country.languages)) ||
                        "NA"}
                    </p>
                  </div>
                </div>
                <div className="borders">
                  <div className="borderCountry">Border Countries: </div>
                  <div className="neighbour">
                    {(country.borders &&
                      country.borders.map((border) => {
                        return (
                          <Link
                            className={`borderLink ${
                              isDarkMode ? "whiteFont" : ""
                            }`}
                            to={`/country/${border}`}
                            key={border}
                          >
                            {" "}
                            <span
                              className={`shadow ${
                                isDarkMode ? "darkMode" : ""
                              }`}
                            >
                              {border}
                            </span>
                          </Link>
                        );
                      })) ||
                      "NA"}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </>
      )}
    </>
  );
}
